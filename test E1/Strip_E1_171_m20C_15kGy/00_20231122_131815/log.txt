[13:18:15] [INFO ]  	
[13:18:15] [INFO ]  ------------------------------------------
[13:18:15] [INFO ]  Running all 3 measurements of the silicon! :)
[13:18:15] [INFO ]  ------------------------------------------
[13:18:15] [INFO ]  Measurement of a dummy I-V curve. 
[13:18:15] [INFO ]  	
[13:18:15] [INFO ]  Start date          : 2023/11/22 13:18:15
[13:18:15] [INFO ]  User                : hgsensor
[13:18:15] [INFO ]  Host                : pcdttp02
[13:18:15] [INFO ]  Identifier          : Strip_E1_171_m20C_15kGy
[13:18:15] [INFO ]  Log will be stored to logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/log.txt
[13:18:15] [INFO ]  	
[13:18:15] [INFO ]  Initialising device.
[13:18:15] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 2410,4005154,C32   Oct  4 2010 14:20:11/A02  /J/K

[13:18:15] [INFO ]  Initialising device.
[13:18:15] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 2410,4354538,C34 Sep 21 2016 15:30:00/A02  /K/M

[13:18:15] [INFO ]  Initialising device.
[13:18:15] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 7001,1293707,A10  /A02  

[13:18:15] [INFO ]  Reseting device.
[13:18:15] [INFO ]  Initialising device.
[13:18:15] [INFO ]  Agilent Technologies,E4980A,MY46414370,A.06.15

[13:18:15] [INFO ]  Initialising device.
[13:18:15] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 6487,1335907,B02   Sep 08 2010 12:47:38/A02  /C/G

[13:18:16] [INFO ]  CV Sweep

[13:18:16] [INFO ]  Measurement Settings:
[13:18:16] [INFO ]  Power Supply voltage limit:      2.10E+01 V
[13:18:16] [INFO ]  Power Supply current limit:      1.00E-04 A
[13:18:16] [INFO ]  LCR measurement voltage:         5.00E-01 V
[13:18:16] [INFO ]  LCR measurement frequency:       1.00E+03 Hz
[13:18:16] [INFO ]  Voltage Delay:                      10.00 s
[13:18:16] [INFO ]  Nominal Voltage [V]	 Measured Voltage [V]	Freq [Hz]	R [Ohm]	R_Err [Ohm]	X [Ohm]	X_Err [Ohm]	Cs [F]	Cp [F]	Total Current [A]
[13:18:16] [INFO ]  

STARTING CV SCAN...


[13:18:40] [INFO ]  Reseting device.
[13:18:40] [INFO ]  Nominal Voltage [V]	 Measured Voltage [V]	Freq [Hz]	R [Ohm]	R_Err [Ohm]	X [Ohm]	X_Err [Ohm]	Cs [F]	Cp [F]	Total Current [A]
[13:19:03] [INFO ]  -1.00E+02	-1.00E+02	1.00E+04	1.03E+08	1.26E+01	-3.07E+05	1.36E+01	5.190E-11	5.190E-11	-4.47E-06
[13:19:17] [INFO ]  -1.00E+02	-1.00E+02	1.00E+05	4.65E+06	6.25E-02	-3.08E+04	7.49E-02	5.170E-11	5.170E-11	-6.36E-06
[13:19:30] [INFO ]  -1.00E+02	-1.00E+02	1.00E+06	2.10E+05	3.35E-03	-3.14E+03	6.60E-03	5.076E-11	5.074E-11	-7.69E-06
[13:19:49] [INFO ]  -1.50E+02	-1.50E+02	1.00E+04	9.55E+07	1.10E+01	-3.07E+05	5.37E+00	5.191E-11	5.191E-11	-1.29E-05
[13:20:02] [INFO ]  -1.50E+02	-1.50E+02	1.00E+05	4.17E+06	6.57E-02	-3.08E+04	4.86E-02	5.169E-11	5.169E-11	-1.33E-05
[13:20:15] [INFO ]  -1.50E+02	-1.50E+02	1.00E+06	2.23E+05	4.95E-03	-3.14E+03	7.49E-03	5.069E-11	5.068E-11	-1.30E-05
[13:20:34] [INFO ]  -2.00E+02	-2.00E+02	1.00E+04	9.16E+07	3.99E+00	-3.06E+05	6.34E+00	5.193E-11	5.193E-11	-1.95E-05
[13:20:48] [INFO ]  -2.00E+02	-2.00E+02	1.00E+05	3.76E+06	3.94E-02	-3.08E+04	1.17E-01	5.165E-11	5.165E-11	-1.90E-05
[13:21:01] [INFO ]  -2.00E+02	-2.00E+02	1.00E+06	2.49E+05	5.76E-03	-3.14E+03	7.75E-03	5.064E-11	5.063E-11	-1.95E-05
[13:21:19] [INFO ]  -2.50E+02	-2.50E+02	1.00E+04	9.29E+07	5.21E+00	-3.07E+05	8.92E+00	5.192E-11	5.192E-11	-2.71E-05
[13:21:33] [INFO ]  -2.50E+02	-2.50E+02	1.00E+05	3.51E+06	6.44E-02	-3.09E+04	6.33E-02	5.157E-11	5.157E-11	-2.72E-05
[13:21:46] [INFO ]  -2.50E+02	-2.50E+02	1.00E+06	2.79E+05	4.13E-03	-3.14E+03	7.77E-03	5.062E-11	5.062E-11	-2.62E-05
[13:22:04] [INFO ]  -3.00E+02	-3.00E+02	1.00E+04	8.35E+07	1.15E+01	-3.07E+05	1.07E+01	5.189E-11	5.189E-11	-3.53E-05
[13:22:18] [INFO ]  -3.00E+02	-3.00E+02	1.00E+05	3.38E+06	5.88E-02	-3.09E+04	8.58E-02	5.148E-11	5.148E-11	-3.52E-05
[13:22:31] [INFO ]  -3.00E+02	-3.00E+02	1.00E+06	3.06E+05	7.04E-03	-3.14E+03	5.49E-03	5.062E-11	5.062E-11	-3.54E-05
[13:22:50] [INFO ]  -3.50E+02	-3.50E+02	1.00E+04	7.27E+07	8.22E+00	-3.07E+05	6.34E+00	5.183E-11	5.183E-11	-4.39E-05
[13:23:04] [INFO ]  -3.50E+02	-3.50E+02	1.00E+05	3.47E+06	4.37E-02	-3.10E+04	1.51E-01	5.135E-11	5.135E-11	-4.65E-05
[13:23:16] [INFO ]  -3.50E+02	-3.50E+02	1.00E+06	3.34E+05	4.46E-03	-3.14E+03	6.01E-03	5.061E-11	5.061E-11	-4.97E-05
[13:23:35] [INFO ]  -4.00E+02	-4.00E+02	1.00E+04	6.36E+07	1.28E+01	-3.07E+05	1.27E+01	5.177E-11	5.177E-11	-5.99E-05
[13:23:49] [INFO ]  -4.00E+02	-4.00E+02	1.00E+05	3.73E+06	1.19E-01	-3.11E+04	1.57E-01	5.124E-11	5.124E-11	-5.92E-05
[13:24:01] [INFO ]  -4.00E+02	-4.00E+02	1.00E+06	3.57E+05	4.10E-03	-3.15E+03	8.02E-03	5.060E-11	5.060E-11	-5.83E-05
[13:24:20] [INFO ]  -4.50E+02	-4.50E+02	1.00E+04	5.74E+07	6.98E+00	-3.08E+05	6.87E+00	5.171E-11	5.170E-11	-7.37E-05
[13:24:34] [INFO ]  -4.50E+02	-4.49E+02	1.00E+05	4.15E+06	1.89E-01	-3.11E+04	8.77E-02	5.116E-11	5.115E-11	-9.67E-05
[13:24:47] [INFO ]  -4.50E+02	-4.48E+02	1.00E+06	3.75E+05	1.23E-02	-3.15E+03	5.87E-03	5.060E-11	5.060E-11	-1.00E-04
[13:25:05] [INFO ]  -5.00E+02	-4.93E+02	1.00E+04	5.49E+07	4.09E+01	-3.08E+05	8.10E+01	5.164E-11	5.164E-11	-1.00E-04
[13:25:19] [INFO ]  -5.00E+02	-4.57E+02	1.00E+05	3.76E+06	4.43E+00	-3.11E+04	6.26E+00	5.122E-11	5.121E-11	-1.00E-04
[13:25:34] [INFO ]  -5.00E+02	-4.95E+02	1.00E+06	3.76E+05	2.90E-01	-3.15E+03	4.73E-02	5.060E-11	5.060E-11	-9.52E-05
[13:25:57] [INFO ]  -5.50E+02	-4.51E+02	1.00E+04	6.27E+07	2.68E+01	-3.07E+05	5.40E+01	5.179E-11	5.179E-11	-1.00E-04
[13:26:21] [INFO ]  -5.50E+02	-4.49E+02	1.00E+05	3.84E+06	5.19E-01	-3.11E+04	9.53E-01	5.120E-11	5.120E-11	-1.00E-04
[13:26:46] [INFO ]  -5.50E+02	-4.43E+02	1.00E+06	3.67E+05	2.66E-02	-3.15E+03	8.58E-03	5.060E-11	5.060E-11	-1.00E-04
[13:27:16] [INFO ]  -6.00E+02	-4.43E+02	1.00E+04	6.21E+07	1.35E+01	-3.07E+05	2.21E+01	5.178E-11	5.178E-11	-1.00E-04
[13:27:46] [INFO ]  -6.00E+02	-4.42E+02	1.00E+05	3.72E+06	8.41E-01	-3.11E+04	1.69E+00	5.122E-11	5.122E-11	-1.00E-04
[13:28:15] [INFO ]  -6.00E+02	-4.43E+02	1.00E+06	3.57E+05	2.55E-02	-3.15E+03	7.10E-03	5.060E-11	5.060E-11	-1.00E-04
[13:28:51] [INFO ]  -6.50E+02	-4.38E+02	1.00E+04	6.50E+07	7.00E+00	-3.07E+05	8.39E+00	5.181E-11	5.181E-11	-1.00E-04
[13:29:26] [INFO ]  -6.50E+02	-4.41E+02	1.00E+05	3.67E+06	1.36E+00	-3.11E+04	2.81E+00	5.123E-11	5.123E-11	-1.00E-04
[13:30:01] [INFO ]  -6.50E+02	-4.35E+02	1.00E+06	3.63E+05	4.35E-02	-3.15E+03	1.24E-02	5.060E-11	5.060E-11	-1.00E-04
[13:30:41] [INFO ]  -7.00E+02	-4.41E+02	1.00E+04	6.42E+07	9.12E+00	-3.07E+05	1.37E+01	5.180E-11	5.180E-11	-1.00E-04
[13:31:21] [INFO ]  -7.00E+02	-4.37E+02	1.00E+05	3.55E+06	5.72E-01	-3.10E+04	1.27E+00	5.126E-11	5.126E-11	-1.00E-04
[13:32:01] [INFO ]  -7.00E+02	-4.30E+02	1.00E+06	3.56E+05	7.46E-02	-3.15E+03	1.40E-02	5.060E-11	5.060E-11	-1.00E-04
[13:32:48] [INFO ]  -7.50E+02	-4.31E+02	1.00E+04	6.69E+07	1.04E+01	-3.07E+05	1.24E+01	5.182E-11	5.182E-11	-1.00E-04
[13:33:33] [INFO ]  -7.50E+02	-4.22E+02	1.00E+05	3.45E+06	5.63E-01	-3.10E+04	1.65E+00	5.129E-11	5.128E-11	-1.00E-04
[13:34:20] [INFO ]  -7.50E+02	-4.14E+02	1.00E+06	3.46E+05	4.45E-02	-3.15E+03	1.03E-02	5.060E-11	5.060E-11	-1.00E-04
[13:35:13] [INFO ]  -8.00E+02	-4.29E+02	1.00E+04	6.49E+07	1.39E+01	-3.07E+05	2.22E+01	5.182E-11	5.182E-11	-1.00E-04
[13:36:04] [INFO ]  -8.00E+02	-4.24E+02	1.00E+05	3.40E+06	8.10E-01	-3.10E+04	2.08E+00	5.130E-11	5.129E-11	-1.00E-04
[13:36:56] [INFO ]  -8.00E+02	-4.13E+02	1.00E+06	3.46E+05	9.24E-02	-3.15E+03	1.54E-02	5.060E-11	5.060E-11	-1.00E-04
[13:37:55] [INFO ]  -8.50E+02	-4.17E+02	1.00E+04	6.77E+07	8.79E+00	-3.07E+05	1.23E+01	5.185E-11	5.185E-11	-1.00E-04
[13:38:52] [INFO ]  -8.50E+02	-4.12E+02	1.00E+05	3.31E+06	3.12E-01	-3.10E+04	9.65E-01	5.133E-11	5.133E-11	-1.00E-04
[13:39:51] [INFO ]  -8.50E+02	-4.02E+02	1.00E+06	3.42E+05	5.10E-02	-3.15E+03	1.21E-02	5.060E-11	5.060E-11	-1.00E-04
[13:40:55] [INFO ]  -9.00E+02	-4.05E+02	1.00E+04	7.30E+07	7.51E+00	-3.07E+05	1.89E+01	5.188E-11	5.188E-11	-1.00E-04
[13:42:01] [INFO ]  -9.00E+02	-4.02E+02	1.00E+05	3.27E+06	3.67E-01	-3.10E+04	1.21E+00	5.135E-11	5.134E-11	-1.00E-04
[13:43:04] [INFO ]  -9.00E+02	-4.08E+02	1.00E+06	3.43E+05	4.74E-02	-3.15E+03	1.26E-02	5.060E-11	5.060E-11	-1.00E-04
[13:44:10] [INFO ]  Reseting device.
[13:44:12] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/10KHz_cv_Strip_E1_171_m20C_15kGy_Test_name.dat
[13:44:12] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/100KHz_cv_Strip_E1_171_m20C_15kGy_Test_name.dat
[13:44:12] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/1MHz_cv_Strip_E1_171_m20C_15kGy_Test_name.dat
[13:44:12] [INFO ]  

 CV SCAN FINISHED


[13:44:12] [INFO ]  

STARTING IV SCAN...


[13:44:35] [INFO ]  Reseting device.
[13:44:35] [INFO ]  Nominal Voltage [V]	 Measured Voltage [V]	Total current [A]	IS nominal voltage[V]	IS measured voltage[V]	IS current [A]	IS current Error [A]	Ramping PS current[A]
[13:46:03] [INFO ]  -2.00E+02	-2.000E+02	-2.555E-05	-1.00E+00	-1.000E+00	-1.536E-10	2.092E-12	1.264E-10
[13:46:40] [INFO ]  -2.00E+02	-2.000E+02	-2.723E-05	-8.00E-01	-8.000E-01	-1.468E-10	6.333E-12	1.952E-10
[13:47:17] [INFO ]  -2.00E+02	-2.000E+02	-2.782E-05	-6.00E-01	-6.000E-01	-1.438E-10	1.001E-11	1.052E-10
[13:47:54] [INFO ]  -2.00E+02	-2.000E+02	-2.815E-05	-4.00E-01	-4.000E-01	-1.392E-10	8.869E-12	1.227E-11
[13:48:32] [INFO ]  -2.00E+02	-2.000E+02	-2.879E-05	-2.00E-01	-2.000E-01	-1.365E-10	6.055E-12	4.208E-11
[13:49:09] [INFO ]  -2.00E+02	-2.000E+02	-3.019E-05	-2.22E-16	2.105E-05	-1.289E-10	9.637E-12	-4.412E-11
[13:49:46] [INFO ]  -2.00E+02	-2.000E+02	-2.994E-05	2.00E-01	2.000E-01	-1.315E-10	1.145E-11	2.121E-10
[13:50:23] [INFO ]  -2.00E+02	-2.000E+02	-2.975E-05	4.00E-01	4.000E-01	-1.283E-10	7.507E-12	1.639E-10
[13:51:00] [INFO ]  -2.00E+02	-2.000E+02	-2.987E-05	6.00E-01	6.000E-01	-1.271E-10	6.087E-12	7.205E-11
[13:51:38] [INFO ]  -2.00E+02	-2.000E+02	-3.053E-05	8.00E-01	8.000E-01	-1.259E-10	6.303E-12	6.577E-11
[13:52:15] [INFO ]  -2.00E+02	-2.000E+02	-2.959E-05	1.00E+00	1.000E+00	-1.215E-10	9.670E-12	1.007E-10
[13:52:59] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-200_V.dat
[13:52:59] [INFO ]  Elapsed time:
[13:52:59] [INFO ]  503.84129881858826
[13:54:16] [INFO ]  -3.00E+02	-3.000E+02	-5.319E-05	-1.00E+00	-1.000E+00	-1.360E-10	7.982E-12	1.381E-10
[13:54:53] [INFO ]  -3.00E+02	-3.000E+02	-5.340E-05	-8.00E-01	-8.000E-01	-1.368E-10	1.356E-11	6.070E-11
[13:55:30] [INFO ]  -3.00E+02	-3.000E+02	-5.355E-05	-6.00E-01	-6.000E-01	-1.293E-10	9.650E-12	-9.148E-11
[13:56:07] [INFO ]  -3.00E+02	-3.000E+02	-5.530E-05	-4.00E-01	-4.000E-01	-1.239E-10	1.332E-11	2.035E-10
[13:56:45] [INFO ]  -3.00E+02	-3.000E+02	-5.649E-05	-2.00E-01	-2.000E-01	-1.235E-10	5.399E-12	1.166E-10
[13:57:21] [INFO ]  -3.00E+02	-3.000E+02	-5.715E-05	-2.22E-16	2.188E-05	-1.102E-10	1.001E-11	2.235E-10
[13:57:59] [INFO ]  -3.00E+02	-3.000E+02	-5.781E-05	2.00E-01	2.000E-01	-1.146E-10	8.985E-12	-6.222E-12
[13:58:36] [INFO ]  -3.00E+02	-3.000E+02	-5.563E-05	4.00E-01	4.000E-01	-1.149E-10	9.493E-12	8.134E-11
[13:59:13] [INFO ]  -3.00E+02	-3.000E+02	-5.566E-05	6.00E-01	6.000E-01	-1.208E-10	1.638E-11	5.570E-11
[13:59:50] [INFO ]  -3.00E+02	-3.000E+02	-5.594E-05	8.00E-01	8.000E-01	-1.159E-10	1.112E-11	8.208E-11
[14:00:27] [INFO ]  -3.00E+02	-3.000E+02	-5.612E-05	1.00E+00	1.000E+00	-1.111E-10	1.604E-11	4.336E-11
[14:01:11] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-300_V.dat
[14:01:12] [INFO ]  Elapsed time:
[14:01:12] [INFO ]  492.36726570129395
[14:02:29] [INFO ]  -4.00E+02	-4.000E+02	-8.398E-05	-1.00E+00	-1.000E+00	-2.295E-10	5.797E-12	5.376E-11
[14:03:06] [INFO ]  -4.00E+02	-4.000E+02	-8.840E-05	-8.00E-01	-8.000E-01	-2.190E-10	2.995E-12	2.017E-10
[14:03:43] [INFO ]  -4.00E+02	-4.000E+02	-8.570E-05	-6.00E-01	-6.000E-01	-2.149E-10	3.366E-12	1.509E-10
[14:04:20] [INFO ]  -4.00E+02	-4.000E+02	-9.579E-05	-4.00E-01	-4.000E-01	-2.084E-10	5.845E-12	1.268E-11
[14:04:56] [INFO ]  -4.00E+02	-4.000E+02	-9.065E-05	-2.00E-01	-2.000E-01	-1.960E-10	7.392E-12	1.194E-10
[14:05:33] [INFO ]  -4.00E+02	-4.000E+02	-9.701E-05	-2.22E-16	2.229E-05	-1.916E-10	3.611E-12	1.529E-10
[14:06:10] [INFO ]  -4.00E+02	-4.000E+02	-9.809E-05	2.00E-01	2.000E-01	-1.878E-10	1.075E-11	2.555E-10
[14:06:47] [INFO ]  -4.00E+02	-4.000E+02	-9.755E-05	4.00E-01	4.000E-01	-1.703E-10	9.293E-12	1.836E-10
[14:07:24] [INFO ]  -4.00E+02	-4.000E+02	-9.663E-05	6.00E-01	6.000E-01	-1.667E-10	1.027E-11	2.240E-10
[14:08:01] [INFO ]  -4.00E+02	-4.000E+02	-8.246E-05	8.00E-01	8.000E-01	-1.654E-10	1.067E-11	1.033E-10
[14:08:38] [INFO ]  -4.00E+02	-4.000E+02	-8.362E-05	1.00E+00	1.000E+00	-1.551E-10	1.302E-11	6.652E-11
[14:09:21] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-400_V.dat
[14:09:22] [INFO ]  Elapsed time:
[14:09:22] [INFO ]  490.0930862426758
[14:10:39] [INFO ]  -5.00E+02	-4.343E+02	-9.999E-05	-1.00E+00	-1.000E+00	-1.694E-10	3.426E-11	8.163E-11
[14:11:16] [INFO ]  -5.00E+02	-4.323E+02	-9.999E-05	-8.00E-01	-8.000E-01	-2.251E-10	2.632E-11	4.658E-11
[14:11:52] [INFO ]  -5.00E+02	-4.194E+02	-9.999E-05	-6.00E-01	-6.000E-01	-1.823E-10	1.632E-11	2.125E-10
[14:12:29] [INFO ]  -5.00E+02	-4.069E+02	-1.000E-04	-4.00E-01	-4.000E-01	-1.645E-10	9.983E-12	2.343E-10
[14:13:06] [INFO ]  -5.00E+02	-4.120E+02	-9.999E-05	-2.00E-01	-2.000E-01	-1.850E-10	9.754E-12	1.648E-10
[14:13:44] [INFO ]  -5.00E+02	-4.156E+02	-9.999E-05	-2.22E-16	2.312E-05	-1.886E-10	1.345E-11	2.018E-10
[14:14:21] [INFO ]  -5.00E+02	-4.156E+02	-9.999E-05	2.00E-01	2.000E-01	-1.605E-10	2.541E-11	2.899E-10
[14:14:58] [INFO ]  -5.00E+02	-4.124E+02	-9.999E-05	4.00E-01	4.000E-01	-1.844E-10	8.477E-12	7.820E-11
[14:15:35] [INFO ]  -5.00E+02	-4.090E+02	-9.999E-05	6.00E-01	6.000E-01	-1.578E-10	6.813E-12	1.118E-10
[14:16:13] [INFO ]  -5.00E+02	-4.118E+02	-9.999E-05	8.00E-01	8.000E-01	-1.555E-10	7.807E-12	1.926E-10
[14:16:50] [INFO ]  -5.00E+02	-4.096E+02	-9.999E-05	1.00E+00	1.000E+00	-1.552E-10	8.894E-12	1.754E-10
[14:17:33] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-500_V.dat
[14:17:34] [INFO ]  Elapsed time:
[14:17:34] [INFO ]  491.903737783432
[14:19:01] [INFO ]  -6.00E+02	-4.215E+02	-9.999E-05	-1.00E+00	-1.000E+00	-1.945E-10	1.141E-11	-4.036E-11
[14:19:38] [INFO ]  -6.00E+02	-4.340E+02	-9.999E-05	-8.00E-01	-8.000E-01	-1.667E-10	1.523E-11	1.312E-10
[14:20:15] [INFO ]  -6.00E+02	-4.288E+02	-1.000E-04	-6.00E-01	-6.000E-01	-1.537E-10	1.267E-11	1.620E-10
[14:20:53] [INFO ]  -6.00E+02	-4.376E+02	-9.999E-05	-4.00E-01	-4.000E-01	-1.599E-10	1.398E-11	-1.514E-11
[14:21:30] [INFO ]  -6.00E+02	-4.289E+02	-1.000E-04	-2.00E-01	-2.000E-01	-1.770E-10	1.669E-11	1.627E-10
[14:22:06] [INFO ]  -6.00E+02	-4.041E+02	-9.999E-05	-2.22E-16	2.254E-05	-1.606E-10	1.143E-11	1.919E-10
[14:22:44] [INFO ]  -6.00E+02	-4.050E+02	-9.999E-05	2.00E-01	2.000E-01	-1.471E-10	7.998E-12	7.841E-11
[14:23:21] [INFO ]  -6.00E+02	-4.079E+02	-1.000E-04	4.00E-01	4.000E-01	-1.806E-10	1.106E-11	1.525E-10
[14:23:58] [INFO ]  -6.00E+02	-4.079E+02	-9.999E-05	6.00E-01	6.000E-01	-1.484E-10	1.228E-11	3.539E-11
[14:24:35] [INFO ]  -6.00E+02	-4.133E+02	-9.999E-05	8.00E-01	8.000E-01	-1.456E-10	7.353E-12	2.252E-10
[14:25:12] [INFO ]  -6.00E+02	-4.078E+02	-9.999E-05	1.00E+00	1.000E+00	-1.545E-10	1.098E-11	1.290E-10
[14:25:56] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-600_V.dat
[14:25:56] [INFO ]  Elapsed time:
[14:25:56] [INFO ]  502.51437616348267
[14:27:34] [INFO ]  -7.00E+02	-4.234E+02	-9.999E-05	-1.00E+00	-1.000E+00	-1.991E-10	1.511E-11	4.802E-11
[14:28:12] [INFO ]  -7.00E+02	-4.525E+02	-1.000E-04	-8.00E-01	-8.000E-01	-1.724E-10	1.558E-11	3.187E-10
[14:28:49] [INFO ]  -7.00E+02	-4.375E+02	-1.000E-04	-6.00E-01	-6.000E-01	-1.652E-10	1.139E-11	2.215E-10
[14:29:27] [INFO ]  -7.00E+02	-4.319E+02	-9.999E-05	-4.00E-01	-4.000E-01	-1.619E-10	1.627E-11	3.952E-11
[14:30:04] [INFO ]  -7.00E+02	-4.332E+02	-9.999E-05	-2.00E-01	-2.000E-01	-1.610E-10	1.962E-11	2.189E-11
[14:30:41] [INFO ]  -7.00E+02	-4.345E+02	-1.000E-04	-2.22E-16	2.172E-05	-1.397E-10	2.454E-11	7.597E-11
[14:31:17] [INFO ]  -7.00E+02	-4.171E+02	-9.999E-05	2.00E-01	2.000E-01	-1.607E-10	1.234E-11	1.890E-10
[14:31:55] [INFO ]  -7.00E+02	-4.191E+02	-9.999E-05	4.00E-01	4.000E-01	-1.448E-10	1.120E-11	2.848E-10
[14:32:32] [INFO ]  -7.00E+02	-4.060E+02	-9.999E-05	6.00E-01	6.000E-01	-1.444E-10	8.240E-12	1.357E-10
[14:33:09] [INFO ]  -7.00E+02	-4.082E+02	-9.999E-05	8.00E-01	8.000E-01	-1.529E-10	8.884E-12	3.246E-11
[14:33:46] [INFO ]  -7.00E+02	-4.114E+02	-9.999E-05	1.00E+00	1.000E+00	-1.359E-10	9.000E-12	1.668E-10
[14:34:30] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-700_V.dat
[14:34:30] [INFO ]  Elapsed time:
[14:34:30] [INFO ]  514.1980710029602
[14:36:18] [INFO ]  -8.00E+02	-4.080E+02	-1.000E-04	-1.00E+00	-1.000E+00	-1.753E-10	1.063E-11	3.696E-11
[14:36:55] [INFO ]  -8.00E+02	-4.088E+02	-9.999E-05	-8.00E-01	-8.000E-01	-1.677E-10	4.226E-12	2.091E-10
[14:37:32] [INFO ]  -8.00E+02	-4.098E+02	-9.999E-05	-6.00E-01	-6.000E-01	-1.646E-10	8.775E-12	3.390E-10
[14:38:09] [INFO ]  -8.00E+02	-4.115E+02	-9.999E-05	-4.00E-01	-4.000E-01	-1.541E-10	4.743E-12	8.245E-11
[14:38:46] [INFO ]  -8.00E+02	-4.132E+02	-1.000E-04	-2.00E-01	-2.000E-01	-1.513E-10	1.038E-11	7.622E-11
[14:39:23] [INFO ]  -8.00E+02	-4.152E+02	-9.999E-05	-2.22E-16	2.213E-05	-1.326E-10	7.312E-12	1.381E-10
[14:40:01] [INFO ]  -8.00E+02	-4.141E+02	-9.999E-05	2.00E-01	2.000E-01	-1.509E-10	7.952E-12	3.395E-11
[14:40:38] [INFO ]  -8.00E+02	-4.032E+02	-9.999E-05	4.00E-01	4.000E-01	-1.344E-10	5.392E-12	8.274E-11
[14:41:15] [INFO ]  -8.00E+02	-4.038E+02	-1.000E-04	6.00E-01	6.000E-01	-1.261E-10	7.124E-12	1.893E-10
[14:41:53] [INFO ]  -8.00E+02	-4.086E+02	-9.999E-05	8.00E-01	8.000E-01	-1.298E-10	7.448E-12	8.633E-11
[14:42:30] [INFO ]  -8.00E+02	-4.147E+02	-9.999E-05	1.00E+00	1.000E+00	-1.223E-10	7.294E-12	1.307E-10
[14:43:14] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-800_V.dat
[14:43:15] [INFO ]  Elapsed time:
[14:43:15] [INFO ]  524.5341300964355
[14:45:14] [INFO ]  -9.00E+02	-4.062E+02	-9.999E-05	-1.00E+00	-1.000E+00	-1.487E-10	5.997E-12	4.158E-11
[14:45:50] [INFO ]  -9.00E+02	-4.035E+02	-9.999E-05	-8.00E-01	-8.000E-01	-1.693E-10	7.921E-12	2.701E-11
[14:46:27] [INFO ]  -9.00E+02	-4.069E+02	-9.999E-05	-6.00E-01	-6.000E-01	-1.479E-10	5.278E-12	-1.239E-10
[14:47:05] [INFO ]  -9.00E+02	-4.045E+02	-9.999E-05	-4.00E-01	-4.000E-01	-1.443E-10	6.550E-12	4.464E-11
[14:47:42] [INFO ]  -9.00E+02	-4.095E+02	-9.999E-05	-2.00E-01	-2.000E-01	-1.459E-10	6.316E-12	5.785E-11
[14:48:19] [INFO ]  -9.00E+02	-4.078E+02	-1.000E-04	-2.22E-16	1.949E-05	-1.310E-10	4.268E-12	9.327E-11
[14:48:56] [INFO ]  -9.00E+02	-4.097E+02	-9.999E-05	2.00E-01	2.000E-01	-1.295E-10	4.923E-12	2.656E-10
[14:49:33] [INFO ]  -9.00E+02	-4.137E+02	-9.999E-05	4.00E-01	4.000E-01	-1.411E-10	5.263E-12	-1.035E-11
[14:50:10] [INFO ]  -9.00E+02	-4.154E+02	-9.999E-05	6.00E-01	6.000E-01	-1.309E-10	8.518E-12	9.492E-11
[14:50:47] [INFO ]  -9.00E+02	-4.152E+02	-9.999E-05	8.00E-01	8.000E-01	-1.275E-10	9.535E-12	1.004E-11
[14:51:25] [INFO ]  -9.00E+02	-4.116E+02	-1.000E-04	1.00E+00	1.000E+00	-1.271E-10	7.952E-12	1.121E-10
[14:52:08] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/iv_Strip_E1_171_m20C_15kGy_Test_name_-900_V.dat
[14:52:09] [INFO ]  Elapsed time:
[14:52:09] [INFO ]  533.9523723125458
[14:53:14] [INFO ]  Reseting device.
[14:53:14] [INFO ]  Saving output to file logs/Strip_E1_171_m20C_15kGy/00_20231122_131815/rv_Strip_E1_171_m20C_15kGy_Test_name.dat
[14:53:14] [INFO ]  

 IV SCAN FINISHED


[14:53:14] [INFO ]  	
[14:53:14] [INFO ]  Cleaning up.
[14:53:14] [INFO ]  	
[14:53:14] [INFO ]  	
