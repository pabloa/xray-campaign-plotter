[17:31:54] [INFO ]  	
[17:31:54] [INFO ]  ------------------------------------------
[17:31:54] [INFO ]  Running all 3 measurements of the silicon! :)
[17:31:54] [INFO ]  ------------------------------------------
[17:31:54] [INFO ]  Measurement of a dummy I-V curve. 
[17:31:54] [INFO ]  	
[17:31:54] [INFO ]  Start date          : 2023/11/20 17:31:54
[17:31:54] [INFO ]  User                : hgsensor
[17:31:54] [INFO ]  Host                : pcdttp02
[17:31:54] [INFO ]  Identifier          : Strip_182_m20C_0kGy
[17:31:54] [INFO ]  Log will be stored to logs/Strip_182_m20C_0kGy/00_20231120_173154/log.txt
[17:31:54] [INFO ]  	
[17:31:54] [INFO ]  Initialising device.
[17:31:54] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 2410,4005154,C32   Oct  4 2010 14:20:11/A02  /J/K

[17:31:54] [INFO ]  Initialising device.
[17:31:54] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 2410,4354538,C34 Sep 21 2016 15:30:00/A02  /K/M

[17:31:54] [INFO ]  Initialising device.
[17:31:54] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 7001,1293707,A10  /A02  

[17:31:54] [INFO ]  Reseting device.
[17:31:54] [INFO ]  Initialising device.
[17:31:54] [INFO ]  Agilent Technologies,E4980A,MY46414370,A.06.15

[17:31:54] [INFO ]  Initialising device.
[17:31:54] [INFO ]  KEITHLEY INSTRUMENTS INC.,MODEL 6487,1335907,B02   Sep 08 2010 12:47:38/A02  /C/G

[17:31:54] [INFO ]  CV Sweep

[17:31:54] [INFO ]  Measurement Settings:
[17:31:54] [INFO ]  Power Supply voltage limit:      2.10E+01 V
[17:31:54] [INFO ]  Power Supply current limit:      1.00E-04 A
[17:31:54] [INFO ]  LCR measurement voltage:         5.00E-01 V
[17:31:54] [INFO ]  LCR measurement frequency:       1.00E+03 Hz
[17:31:54] [INFO ]  Voltage Delay:                      10.00 s
[17:31:54] [INFO ]  Nominal Voltage [V]	 Measured Voltage [V]	Freq [Hz]	R [Ohm]	R_Err [Ohm]	X [Ohm]	X_Err [Ohm]	Cs [F]	Cp [F]	Total Current [A]
[17:31:54] [INFO ]  

STARTING CV SCAN...


[17:32:17] [INFO ]  Reseting device.
[17:32:17] [INFO ]  Nominal Voltage [V]	 Measured Voltage [V]	Freq [Hz]	R [Ohm]	R_Err [Ohm]	X [Ohm]	X_Err [Ohm]	Cs [F]	Cp [F]	Total Current [A]
[17:32:40] [INFO ]  -1.00E+02	-1.00E+02	1.00E+04	4.11E+08	6.80E+00	-3.14E+05	8.16E+00	5.075E-11	5.075E-11	-3.56E-10
[17:32:53] [INFO ]  -1.00E+02	-1.00E+02	1.00E+05	2.08E+07	6.87E-02	-3.14E+04	6.03E-02	5.072E-11	5.072E-11	4.35E-10
[17:33:06] [INFO ]  -1.00E+02	-1.00E+02	1.00E+06	5.66E+05	7.32E-03	-3.16E+03	9.32E-03	5.044E-11	5.044E-11	5.11E-10
[17:33:24] [INFO ]  -1.50E+02	-1.50E+02	1.00E+04	4.49E+08	7.54E+00	-3.13E+05	9.11E+00	5.081E-11	5.081E-11	2.34E-10
[17:33:37] [INFO ]  -1.50E+02	-1.50E+02	1.00E+05	2.06E+07	7.83E-02	-3.13E+04	8.39E-02	5.079E-11	5.079E-11	4.59E-10
[17:33:50] [INFO ]  -1.50E+02	-1.50E+02	1.00E+06	5.43E+05	8.88E-03	-3.15E+03	7.75E-03	5.050E-11	5.050E-11	2.12E-10
[17:34:08] [INFO ]  -2.00E+02	-2.00E+02	1.00E+04	4.20E+08	6.83E+00	-3.13E+05	9.16E+00	5.085E-11	5.085E-11	3.00E-10
[17:34:21] [INFO ]  -2.00E+02	-2.00E+02	1.00E+05	2.04E+07	9.97E-02	-3.13E+04	7.54E-02	5.083E-11	5.083E-11	2.62E-10
[17:34:33] [INFO ]  -2.00E+02	-2.00E+02	1.00E+06	5.30E+05	7.05E-03	-3.15E+03	8.10E-03	5.054E-11	5.054E-11	5.75E-10
[17:34:51] [INFO ]  -2.50E+02	-2.50E+02	1.00E+04	4.29E+08	9.77E+00	-3.13E+05	7.64E+00	5.088E-11	5.088E-11	2.69E-10
[17:35:04] [INFO ]  -2.50E+02	-2.50E+02	1.00E+05	2.01E+07	6.03E-02	-3.13E+04	1.10E-01	5.086E-11	5.086E-11	3.57E-10
[17:35:17] [INFO ]  -2.50E+02	-2.50E+02	1.00E+06	5.21E+05	7.86E-03	-3.15E+03	7.08E-03	5.057E-11	5.057E-11	6.24E-10
[17:35:35] [INFO ]  -3.00E+02	-3.00E+02	1.00E+04	4.40E+08	9.30E+00	-3.13E+05	7.42E+00	5.090E-11	5.090E-11	5.70E-10
[17:35:47] [INFO ]  -3.00E+02	-3.00E+02	1.00E+05	1.99E+07	5.26E-02	-3.13E+04	7.72E-02	5.088E-11	5.088E-11	3.70E-10
[17:36:00] [INFO ]  -3.00E+02	-3.00E+02	1.00E+06	5.15E+05	9.90E-03	-3.15E+03	7.59E-03	5.059E-11	5.059E-11	5.36E-10
[17:36:18] [INFO ]  -3.50E+02	-3.50E+02	1.00E+04	4.31E+08	5.29E+00	-3.13E+05	7.65E+00	5.090E-11	5.090E-11	5.12E-10
[17:36:31] [INFO ]  -3.50E+02	-3.50E+02	1.00E+05	2.00E+07	5.60E-02	-3.13E+04	8.06E-02	5.088E-11	5.088E-11	3.95E-10
[17:36:43] [INFO ]  -3.50E+02	-3.50E+02	1.00E+06	5.14E+05	7.28E-03	-3.15E+03	9.59E-03	5.060E-11	5.059E-11	4.97E-10
[17:37:01] [INFO ]  -4.00E+02	-4.00E+02	1.00E+04	4.08E+08	7.32E+00	-3.13E+05	6.91E+00	5.090E-11	5.090E-11	6.73E-10
[17:37:14] [INFO ]  -4.00E+02	-4.00E+02	1.00E+05	2.00E+07	4.39E-02	-3.13E+04	7.97E-02	5.088E-11	5.088E-11	7.24E-10
[17:37:27] [INFO ]  -4.00E+02	-4.00E+02	1.00E+06	5.15E+05	1.01E-02	-3.15E+03	1.10E-02	5.060E-11	5.059E-11	4.62E-10
[17:37:45] [INFO ]  -4.50E+02	-4.50E+02	1.00E+04	4.27E+08	7.13E+00	-3.13E+05	7.23E+00	5.090E-11	5.090E-11	4.40E-10
[17:37:57] [INFO ]  -4.50E+02	-4.50E+02	1.00E+05	2.00E+07	4.65E-02	-3.13E+04	1.09E-01	5.088E-11	5.088E-11	4.74E-10
[17:38:10] [INFO ]  -4.50E+02	-4.50E+02	1.00E+06	5.15E+05	9.33E-03	-3.15E+03	8.13E-03	5.060E-11	5.059E-11	5.49E-10
[17:38:28] [INFO ]  -5.00E+02	-5.00E+02	1.00E+04	4.36E+08	8.46E+00	-3.13E+05	6.55E+00	5.090E-11	5.090E-11	3.25E-10
[17:38:41] [INFO ]  -5.00E+02	-5.00E+02	1.00E+05	2.00E+07	8.50E-02	-3.13E+04	8.69E-02	5.088E-11	5.088E-11	2.74E-10
[17:38:53] [INFO ]  -5.00E+02	-5.00E+02	1.00E+06	5.15E+05	7.04E-03	-3.15E+03	7.90E-03	5.060E-11	5.059E-11	5.97E-10
[17:39:11] [INFO ]  -5.50E+02	-5.50E+02	1.00E+04	4.44E+08	7.21E+00	-3.13E+05	5.85E+00	5.090E-11	5.090E-11	5.29E-10
[17:39:24] [INFO ]  -5.50E+02	-5.50E+02	1.00E+05	2.00E+07	6.07E-02	-3.13E+04	7.46E-02	5.088E-11	5.088E-11	4.93E-10
[17:39:37] [INFO ]  -5.50E+02	-5.50E+02	1.00E+06	5.15E+05	8.73E-03	-3.15E+03	9.82E-03	5.060E-11	5.060E-11	1.77E-10
[17:39:55] [INFO ]  -6.00E+02	-6.00E+02	1.00E+04	4.50E+08	8.55E+00	-3.13E+05	7.81E+00	5.090E-11	5.090E-11	4.44E-10
[17:40:07] [INFO ]  -6.00E+02	-6.00E+02	1.00E+05	2.01E+07	6.38E-02	-3.13E+04	8.54E-02	5.088E-11	5.088E-11	2.69E-10
[17:40:20] [INFO ]  -6.00E+02	-6.00E+02	1.00E+06	5.16E+05	1.09E-02	-3.15E+03	1.08E-02	5.060E-11	5.059E-11	5.30E-10
[17:40:38] [INFO ]  -6.50E+02	-6.50E+02	1.00E+04	4.26E+08	8.38E+00	-3.13E+05	8.04E+00	5.090E-11	5.090E-11	5.65E-10
[17:40:51] [INFO ]  -6.50E+02	-6.50E+02	1.00E+05	2.01E+07	6.64E-02	-3.13E+04	4.35E-02	5.088E-11	5.088E-11	6.24E-10
[17:41:03] [INFO ]  -6.50E+02	-6.50E+02	1.00E+06	5.17E+05	1.22E-02	-3.15E+03	9.38E-03	5.060E-11	5.059E-11	5.12E-10
[17:41:21] [INFO ]  -7.00E+02	-7.00E+02	1.00E+04	3.95E+08	3.67E+00	-3.13E+05	8.19E+00	5.090E-11	5.090E-11	2.87E-10
[17:41:34] [INFO ]  -7.00E+02	-7.00E+02	1.00E+05	2.00E+07	4.55E-02	-3.13E+04	4.43E-02	5.088E-11	5.088E-11	3.21E-10
[17:41:47] [INFO ]  -7.00E+02	-7.00E+02	1.00E+06	5.16E+05	6.26E-03	-3.15E+03	9.08E-03	5.060E-11	5.059E-11	3.40E-10
[17:42:04] [INFO ]  -7.50E+02	-7.50E+02	1.00E+04	4.42E+08	7.95E+00	-3.13E+05	5.92E+00	5.090E-11	5.090E-11	2.32E-10
[17:42:17] [INFO ]  -7.50E+02	-7.50E+02	1.00E+05	2.01E+07	8.01E-02	-3.13E+04	5.38E-02	5.088E-11	5.088E-11	5.13E-10
[17:42:30] [INFO ]  -7.50E+02	-7.50E+02	1.00E+06	5.16E+05	1.08E-02	-3.15E+03	8.63E-03	5.060E-11	5.059E-11	5.74E-10
[17:42:48] [INFO ]  -8.00E+02	-8.00E+02	1.00E+04	4.19E+08	5.10E+00	-3.13E+05	7.31E+00	5.090E-11	5.090E-11	1.52E-10
[17:43:01] [INFO ]  -8.00E+02	-8.00E+02	1.00E+05	2.00E+07	1.06E-01	-3.13E+04	9.06E-02	5.088E-11	5.088E-11	3.26E-10
[17:43:13] [INFO ]  -8.00E+02	-8.00E+02	1.00E+06	5.16E+05	1.08E-02	-3.15E+03	7.62E-03	5.060E-11	5.059E-11	3.44E-11
[17:43:31] [INFO ]  -8.50E+02	-8.50E+02	1.00E+04	4.48E+08	7.05E+00	-3.13E+05	6.23E+00	5.090E-11	5.090E-11	1.28E-10
[17:43:44] [INFO ]  -8.50E+02	-8.50E+02	1.00E+05	2.01E+07	7.33E-02	-3.13E+04	8.74E-02	5.088E-11	5.088E-11	5.94E-10
[17:43:56] [INFO ]  -8.50E+02	-8.50E+02	1.00E+06	5.15E+05	8.62E-03	-3.15E+03	9.49E-03	5.060E-11	5.059E-11	5.32E-10
[17:44:14] [INFO ]  -9.00E+02	-9.00E+02	1.00E+04	4.50E+08	8.75E+00	-3.13E+05	5.69E+00	5.090E-11	5.090E-11	4.19E-10
[17:44:27] [INFO ]  -9.00E+02	-9.00E+02	1.00E+05	2.01E+07	4.77E-02	-3.13E+04	7.87E-02	5.088E-11	5.088E-11	4.58E-10
[17:44:40] [INFO ]  -9.00E+02	-9.00E+02	1.00E+06	5.16E+05	1.03E-02	-3.15E+03	1.14E-02	5.060E-11	5.059E-11	1.02E-10
[17:46:34] [INFO ]  Reseting device.
[17:46:36] [INFO ]  Saving output to file logs/Strip_182_m20C_0kGy/00_20231120_173154/10KHz_cv_Strip_182_m20C_0kGy_Test_name.dat
[17:46:36] [INFO ]  Saving output to file logs/Strip_182_m20C_0kGy/00_20231120_173154/100KHz_cv_Strip_182_m20C_0kGy_Test_name.dat
[17:46:36] [INFO ]  Saving output to file logs/Strip_182_m20C_0kGy/00_20231120_173154/1MHz_cv_Strip_182_m20C_0kGy_Test_name.dat
[17:46:36] [INFO ]  

 CV SCAN FINISHED


[17:46:36] [INFO ]  

STARTING IV SCAN...


[17:46:58] [INFO ]  Reseting device.
[17:46:58] [INFO ]  Nominal Voltage [V]	 Measured Voltage [V]	Total current [A]	IS nominal voltage[V]	IS measured voltage[V]	IS current [A]	IS current Error [A]	Ramping PS current[A]
[17:48:39] [INFO ]  -3.50E+02	-3.500E+02	-3.693E-10	-1.00E+00	-1.000E+00	-2.031E-11	1.125E-11	-2.301E-12
[17:49:14] [INFO ]  -3.50E+02	-3.500E+02	1.921E-10	-8.00E-01	-8.000E-01	-8.818E-12	5.855E-12	1.698E-10
[17:49:49] [INFO ]  -3.50E+02	-3.500E+02	7.262E-10	-6.00E-01	-6.000E-01	-1.592E-11	4.858E-12	1.493E-10
[17:50:24] [INFO ]  -3.50E+02	-3.500E+02	6.060E-10	-4.00E-01	-4.000E-01	-2.697E-11	1.075E-11	2.347E-10
[17:50:59] [INFO ]  -3.50E+02	-3.500E+02	4.330E-10	-2.00E-01	-2.000E-01	-1.218E-11	9.800E-12	1.710E-11
[17:51:34] [INFO ]  -3.50E+02	-3.500E+02	3.420E-10	-2.22E-16	2.461E-05	-2.332E-11	9.106E-12	4.179E-11
[17:52:09] [INFO ]  -3.50E+02	-3.500E+02	7.840E-10	2.00E-01	2.000E-01	-2.358E-11	7.137E-12	-1.195E-10
[17:52:44] [INFO ]  -3.50E+02	-3.500E+02	7.427E-10	4.00E-01	4.000E-01	-2.795E-11	1.137E-11	-6.431E-11
[17:53:19] [INFO ]  -3.50E+02	-3.500E+02	3.965E-10	6.00E-01	6.000E-01	-2.185E-11	5.132E-12	-1.943E-11
[17:53:54] [INFO ]  -3.50E+02	-3.500E+02	3.325E-10	8.00E-01	8.000E-01	-2.204E-11	4.075E-12	-8.731E-11
[17:54:30] [INFO ]  -3.50E+02	-3.500E+02	3.511E-10	1.00E+00	1.000E+00	-1.599E-11	4.683E-12	5.277E-11
[17:55:13] [INFO ]  Saving output to file logs/Strip_182_m20C_0kGy/00_20231120_173154/iv_Strip_182_m20C_0kGy_Test_name_-350_V.dat
[17:55:14] [INFO ]  Elapsed time:
[17:55:14] [INFO ]  495.15508818626404
[17:56:12] [INFO ]  Reseting device.
[17:56:12] [INFO ]  Saving output to file logs/Strip_182_m20C_0kGy/00_20231120_173154/rv_Strip_182_m20C_0kGy_Test_name.dat
[17:56:12] [INFO ]  

 IV SCAN FINISHED


[17:56:12] [INFO ]  	
[17:56:12] [INFO ]  Cleaning up.
[17:56:12] [INFO ]  	
[17:56:12] [INFO ]  	
