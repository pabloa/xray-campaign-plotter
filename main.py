import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator
from matplotlib.cm import coolwarm, ScalarMappable
from matplotlib import gridspec
from matplotlib.pyplot import axhline, subplots, show, hist, figure, setp, colorbar, plot, cm, title, xlabel, ylabel, grid, legend, savefig, axes, pcolormesh, close
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, AutoMinorLocator, MaxNLocator
import matplotlib.colors as colors

from Oxides import Oxide
from Plotter import Plotter

import os
import glob
import numpy as np



'''
#1 E1 y sin CMS
oxides = ["Type C (1)", "Type C (2)", "Type C' (1)", "Type C' (2)", "Type E1 (1)"]
folders = ["Strip_180", "Strip_182", "Strip_417", "Strip_419", "Strip_E1_173"]
lengths = [2.35, 2.35, 2.35, 2.35, 2.35]
colours = ['darkorange', 'darkorange', 'brown', 'brown', 'indigo']
linestyles = ['solid', 'dashed', 'solid', 'dashed', 'solid']
'''

'''
#2 E1 y sin CMS
oxides = ["Type C (1)", "Type C (2)", "Type C' (1)", "Type C' (2)", "Type E1 (1)", "Type E1 (2)"]
folders = ["Strip_180", "Strip_182", "Strip_417", "Strip_419", "Strip_E1_173", "Strip_E1_171"]
lengths = [2.35, 2.35, 2.35, 2.35, 2.35, 2.35]
colours = ['darkorange', 'darkorange', 'brown', 'brown', 'indigo', 'indigo']
linestyles = ['solid', 'dashed', 'solid', 'dashed', 'solid', 'dashed']
markers = ['o', 'o', 'v', 'v', '^', '^']
'''

#1 E1 con CMS
oxides = ["Type C (1)", "Type C (2)", "Type C' (1)", "Type C' (2)", "Type E1 (1)", "CMS Tracker"]#, "Type C (C)"]#, "Type E1 (A)", "Type E1 (B)"]
folders = ["Strip_180", "Strip_182", "Strip_417", "Strip_419", "Strip_E1_173", "Strip_Tracker_515565_01"]#, "Strip_C_421"]#, "Strip_E1_171", "Strip_E1_173"]
lengths = [2.35, 2.35, 2.35, 2.35, 2.35, 1.55]
colours = ['darkorange', 'darkorange', 'brown', 'brown', 'indigo', 'blue']#, 'darkorange']
linestyles = ['solid', 'dashed', 'solid', 'dashed', 'solid', 'solid']#, 'dotted']
markers = ['o', 'o', 'v', 'v', '^', 's']


#Format: frequency: [[R_open, X_open], [R_Short, X_short]]
correction = {1e4: [[2.1170495e+02, -3.1941629e+05], [1.4886421, -10.322904 ]], 1e5: [[1.2350204e+01, -3.1948538e+04], [2.0442465, 2.0464124]], 1e6: [[1.6571294e+00, -3.2034364e+03], [4.2284494, 24.491841]]}


interestingVoltages = [-300, -400, -500, -600, -700, -800, -900]
interestingDosis = [15, 30, 70, 100, 200]
#interestingDosis = [100, 200, 500, 1000]
interestingFreqs = [1e4, 1e5, 1e6]



'''
typeCA = Oxide(oxides[0], folders[0])
typeCB = Oxide(oxides[1], folders[1])
typeCpA = Oxide(oxides[2], folders[2])
typeCpB = Oxide(oxides[3], folders[3])
typeTracker = Oxide(oxides[4], folders[4])

typeCA = Oxide(oxides[0], folders[0])
#p.plotRV(typeCA)
p.plotRDosis(typeCA, interestingVoltages)
'''

p = Plotter()

fullAnalysis = []
for i in range(len(oxides)):
    fullAnalysis.append(Oxide(oxides[i], lengths[i], folders[i], correction))
fullAnalysis = np.array(fullAnalysis)

for o in fullAnalysis:
    p.plotCV_freq(o)
    p.plotCDosis(o, 1e6, interestingVoltages)
    p.plotRV(o)
    p.plotRDosis(o, interestingVoltages)
    for d in o.dosis:
        p.plotIV_Bias(o, d, interestingVoltages)
    for v in interestingVoltages:
        p.plotIV_Dosis(o, o.dosis, v)

for v in interestingVoltages:
    p.plotCDosis_multi(fullAnalysis, colours, linestyles, markers, 1e6, v)
    for d in interestingDosis:
        p.plotCV_freq_multi(fullAnalysis, colours, linestyles, markers, interestingFreqs, v, d)
    p.plotRDosis_multi(fullAnalysis, colours, linestyles, markers, v)

for d in interestingDosis:
    p.plotCV_multi(fullAnalysis, colours, linestyles, markers, 1e6, d)
    p.plotRV_multi(fullAnalysis, colours, linestyles, markers, d)