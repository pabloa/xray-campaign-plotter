import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator
from matplotlib.cm import coolwarm, ScalarMappable
from matplotlib import gridspec
from matplotlib.pyplot import axhline, subplots, show, hist, figure, setp, colorbar, plot, cm, title, xlabel, ylabel, grid, legend, savefig, axes, pcolormesh, close
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, AutoMinorLocator, MaxNLocator
import matplotlib.colors as colors
from matplotlib.lines import Line2D

import numpy as np

from Oxides import Oxide

class Plotter:
    
    def __init__(self):
        self.route = "plots/"
    

    def plotCV_freq(self, ox):
        for d in range(len(ox.cvResults)):
            fig, ax1 = plt.subplots()
            color = plt.cm.jet(np.linspace(0,1,len(ox.cvResults[d])))  
            for f in range(len(ox.cvResults[d])):
                ax1.errorbar(-ox.cvResults[d,f,0,:], ox.cvResults[d,f,2,:]/ox.length, yerr=ox.cvResults[d,f,3,:]/ox.length, fmt='o', color=color[f])
                ax1.plot(-ox.cvResults[d,f,0,:], ox.cvResults[d,f,2,:]/ox.length, label=(str(int(ox.cvResults[d,f,1,0]/1e3))+" kHz"), color=color[f])
            ax1.set_title(ox.name+" - Dosis: "+str(ox.dosis[d])+"kGy", fontsize=16)
            ax1.set_ylabel("Interstrip capacitance [F/cm]", fontsize=12)
            ax1.set_xlabel("Bias voltage [V]", fontsize=12)
            ax1.tick_params(axis='both', which='major', labelsize=11)
            ax1.tick_params(axis='both', which='minor', labelsize=11)
            #ax1.tick_params(axis='y', labelcolor=color)
            ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            ax1.grid()
            #ax1.set_yscale('log')
            fig.tight_layout()
            plt.legend(loc="upper right", prop={'size': 12})
            #plt.show()
            fig.savefig(self.route+"capacitance/"+ox.name+"_"+str(ox.dosis[d])+"kGy_CV_Curve"+'.png')



    def plotCV_freq_multi(self, oxis, color, lines, markers, interestingFreqs, biasV, dosis):
        fig, ax1 = plt.subplots()
        #color = plt.cm.jet(np.linspace(0,1,len(oxis)))
        fname = ""
        nVs = 0
        legend_elements = []
        for ox in oxis:
            if dosis in ox.dosis:
                valuesForF = []
                errorsForF = []
                for freq in interestingFreqs:
                    indexDosis = ox.dosis.index(dosis)
                    indexFreq = (np.abs(ox.cvResults[indexDosis, :, 1, 0] - freq)).argmin()
                    indexBiasV = (np.abs(ox.cvResults[indexDosis, indexFreq, 0,:] - biasV)).argmin()
                    valuesForF.append(ox.cvResults[indexDosis, indexFreq,2,indexBiasV])
                    errorsForF.append(ox.cvResults[indexDosis, indexFreq,3,indexBiasV])
                ax1.errorbar(interestingFreqs, np.array(valuesForF)/ox.length, yerr=np.array(errorsForF)/ox.length, fmt=markers[nVs], color=color[nVs])
                ax1.plot(interestingFreqs, np.array(valuesForF)/ox.length, label=(ox.name), color=color[nVs], linestyle=lines[nVs])
                legend_elements.append(Line2D([0], [0], c=color[nVs], marker=markers[nVs], label=ox.name))
                nVs+=1
                fname+=ox.name
                fname+=','
        ax1.set_title("Different oxides - Bias V: "+str(biasV)+"V - Dosis: "+str(dosis)+"kGy", fontsize=16)
        ax1.set_ylabel("Interstrip capacitance [F/cm]", fontsize=12)
        ax1.set_xlabel("Frequency [kHz]", fontsize=12)
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        ax1.set_xscale('log')
        fig.tight_layout()
        plt.legend(handles=legend_elements, loc="upper right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"capacitance/"+fname+"_"+str(biasV)+"V_"+str(dosis)+"kGy_fV_Curve"+'.png')
        plt.close(fig)



    def plotCV_multi(self, oxis, color, lines, markers, freq, dosis):
        
        fig, ax1 = plt.subplots()
        #color = plt.cm.jet(np.linspace(0,1,len(oxis))) 
        nOxis = 0
        fname = ""
        legend_elements = []
        for ox in oxis:
            if dosis in ox.dosis:
                indexDosis = ox.dosis.index(dosis)
                indexFreq = np.where(ox.cvResults[indexDosis] == freq)[0][0]
                ax1.errorbar(-ox.cvResults[indexDosis,indexFreq,0,:], ox.cvResults[indexDosis,indexFreq,2,:]/ox.length, yerr=ox.cvResults[indexDosis,indexFreq,3,:]/ox.length, fmt=markers[nOxis], color=color[nOxis])
                ax1.plot(-ox.cvResults[indexDosis,indexFreq,0,:], ox.cvResults[indexDosis,indexFreq,2,:]/ox.length, label=(ox.name), color=color[nOxis], linestyle=lines[nOxis])
                legend_elements.append(Line2D([0], [0], c=color[nOxis], marker=markers[nOxis], label=ox.name))
                nOxis +=1
            fname+=ox.name
            fname+=','
        ax1.set_title("Different oxides - Dosis: "+str(dosis)+"kGy - Freq: "+str(int(freq/1e3))+"kHz", fontsize=16)
        ax1.set_ylabel("Interstrip capacitance [F/cm]", fontsize=12)
        ax1.set_xlabel("Bias voltage [V]", fontsize=12)
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        #ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(handles=legend_elements, loc="upper right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"capacitance/"+fname+"_"+str(dosis)+"kGy_"+str(int(freq/1e3))+"kHz_CV_Curve"+'.png')
        plt.close(fig)


    def plotCDosis(self, ox, freq, biasVs):
        
        fig, ax1 = plt.subplots()
        color = plt.cm.jet(np.linspace(0,1,len(biasVs))) 
        nVs = 0
        
        for v in biasVs:
            valuesForDosis = []
            errorsForDosis = []
            for d in range(len(ox.cvResults)):
                indexFreq = np.where(ox.cvResults[d] == freq)[0][0]
                indexBiasV = (np.abs(ox.cvResults[d, indexFreq, 0,:] - v)).argmin()
                valuesForDosis.append(ox.cvResults[d,indexFreq,2,indexBiasV])
                errorsForDosis.append(ox.cvResults[d,indexFreq,3,indexBiasV])
            ax1.errorbar(ox.dosis, np.array(valuesForDosis)/ox.length, yerr=np.array(errorsForDosis)/ox.length, fmt='o', color=color[nVs])
            ax1.plot(ox.dosis, np.array(valuesForDosis)/ox.length, label=(str(v)+"V"), color=color[nVs])
            nVs+=1
        ax1.set_title(ox.name+" - Freq: "+str(int(freq/1e3))+"kHz", fontsize=16)
        ax1.set_ylabel("Interstrip capacitance [F/cm]", fontsize=12)
        ax1.set_xlabel("Radiation Dose [kGy]", fontsize=12)
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        #ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(loc="upper right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"capacitance/"+ox.name+"_"+str(int(freq/1e3))+"kHz_CD_Curve"+'.png')
        plt.close(fig)


    def plotCDosis_multi(self, oxis, color, lines, markers, freq, biasV):
        
        fig, ax1 = plt.subplots()
        #color = plt.cm.jet(np.linspace(0,1,len(oxis))) 
        fname = ""
        nVs = 0
        legend_elements = []
        for ox in oxis:
            valuesForDosis = []
            errorsForDosis = []
            for d in range(len(ox.cvResults)):
                indexFreq = np.where(ox.cvResults[d] == freq)[0][0]
                indexBiasV = (np.abs(ox.cvResults[d, indexFreq, 0,:] - biasV)).argmin()
                valuesForDosis.append(ox.cvResults[d,indexFreq,2,indexBiasV])
                errorsForDosis.append(ox.cvResults[d,indexFreq,3,indexBiasV])
            ax1.errorbar(ox.dosis, np.array(valuesForDosis)/ox.length, yerr=np.array(errorsForDosis)/ox.length, fmt=markers[nVs], color=color[nVs])
            ax1.plot(ox.dosis, np.array(valuesForDosis)/ox.length, label=(ox.name), color=color[nVs], linestyle=lines[nVs])
            legend_elements.append(Line2D([0], [0], c=color[nVs], marker=markers[nVs], label=ox.name))
            nVs+=1
            fname+=ox.name
            fname+=','
        ax1.set_title("Different oxides - Bias V: "+str(biasV)+"V - Freq: "+str(int(freq/1e3))+"kHz", fontsize=16)
        ax1.set_ylabel("Interstrip capacitance [F/cm]", fontsize=12)
        ax1.set_xlabel("Radiation Dose [kGy]", fontsize=12)
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        #ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(handles=legend_elements, loc="upper right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"capacitance/"+fname+"_"+str(biasV)+"V_"+str(int(freq/1e3))+"kHz_CD_Curve"+'.png')
        plt.close(fig)



    '''
    RESISTANCES
    '''

    def plotRV(self, ox):
        fig, ax1 = plt.subplots()
        color = plt.cm.jet(np.linspace(0,1,len(ox.rvResults)))
        for d in range(len(ox.rvResults)):
            ax1.errorbar(-ox.rvResults[d,:,0], ox.rvResults[d,:,1]*ox.length, yerr=ox.rvResults[d,:,2]*ox.length, fmt='o', color=color[d])
            ax1.plot(-ox.rvResults[d,:,0], ox.rvResults[d,:,1]*ox.length, color=color[d], label=(str(ox.dosis[d])+"kGy"))
        ax1.hlines(y=1e8, xmin=0, xmax=ox.dosis[-1]*1.02, linewidth=2, color='r', linestyle="dashed")
        ax1.set_title(ox.name, fontsize=16)
        ax1.set_ylabel("Interstrip resistance [Ω·cm]", fontsize=12)
        ax1.set_xlabel("Bias voltage [V]", fontsize=12)
        ax1.set_ylim([0.5e8, 1e9])
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(loc="lower right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"resistance/"+ox.name+"_RV_Curve"+'.png')
        plt.close(fig)



    def plotRV_multi(self, oxis, color, lines, markers, dosis):
        
        fig, ax1 = plt.subplots()
        #color = plt.cm.jet(np.linspace(0,1,len(oxis))) 
        nOxis = 0
        fname = ""
        legend_elements = []
        for ox in oxis:
            if dosis in ox.dosis:
                indexDosis = ox.dosis.index(dosis)
                ax1.errorbar(-ox.rvResults[indexDosis,:,0], ox.rvResults[indexDosis,:,1]*ox.length, yerr=ox.rvResults[indexDosis,:,2]*ox.length, fmt=markers[nOxis], color=color[nOxis], linestyle=lines[nOxis])
                ax1.plot(-ox.rvResults[indexDosis,:,0], ox.rvResults[indexDosis,:,1]*ox.length, color=color[nOxis], label=(ox.name))
                legend_elements.append(Line2D([0], [0], c=color[nOxis], marker=markers[nOxis], label=ox.name))
                nOxis +=1
            fname+=ox.name
            fname+=','
        ax1.set_title("Different oxides - Dosis: "+str(dosis)+"kGy", fontsize=16)
        ax1.set_ylabel("Interstrip resistance [Ω·cm]", fontsize=12)
        ax1.set_xlabel("Bias voltage [V]", fontsize=12)
        ax1.set_ylim([5e5, 1e14])
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(handles=legend_elements, loc="lower right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"resistance/"+fname+"_"+str(dosis)+"kGy_RV_Curve"+'.png')
        plt.close(fig)


    def plotRDosis(self, ox, biasVs):
        
        fig, ax1 = plt.subplots()
        color = plt.cm.jet(np.linspace(0,1,len(biasVs))) 
        nVs = 0
        
        for v in biasVs:
            valuesForDosis = []
            uncertaintyForDosis = []
            for d in range(len(ox.rvResults)):
                indexBiasV = (np.abs(ox.rvResults[d,:,0] - v)).argmin()
                valuesForDosis.append(ox.rvResults[d,indexBiasV,1])
                uncertaintyForDosis.append(ox.rvResults[d,indexBiasV,2])
            ax1.errorbar(ox.dosis, np.array(valuesForDosis)*ox.length, yerr=np.array(uncertaintyForDosis)*ox.length, fmt='o', color=color[nVs])
            ax1.plot(ox.dosis, np.array(valuesForDosis)*ox.length, color=color[nVs], label=(str(v)+"V"))
            nVs+=1
        ax1.hlines(y=1e8, xmin=0, xmax=ox.dosis[-1]*1.02, linewidth=2, color='r', linestyle="dashed")
        ax1.set_title(ox.name, fontsize=16)
        ax1.set_ylabel("Interstrip resistance [Ω·cm]", fontsize=12)
        ax1.set_xlabel("Radiation Dose [kGy]", fontsize=12)
        ax1.set_ylim([0.5e8, 1e9])
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        ax1.set_xlim([0, ox.dosis[-1]*1.02])
        ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(loc="lower left", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"resistance/"+ox.name+"_RD_Curve"+'.png')
        plt.close(fig)




    def plotRDosis_multi(self, oxis, color, lines, markers, biasV):
        
        fig, ax1 = plt.subplots()
        #color = plt.cm.jet(np.linspace(0,1,len(oxis)))
        fname = ""
        nOxis = 0
        legend_elements = []
        for ox in oxis:
            valuesForDosis = []
            uncertaintyForDosis = []
            for d in range(len(ox.rvResults)):
                indexBiasV = (np.abs(ox.rvResults[d,:,0] - biasV)).argmin()
                valuesForDosis.append(ox.rvResults[d,indexBiasV,1])
                uncertaintyForDosis.append(ox.rvResults[d,indexBiasV,2])
            ax1.errorbar(ox.dosis, np.array(valuesForDosis)*ox.length, yerr=np.array(uncertaintyForDosis)*ox.length, fmt=markers[nOxis], color=color[nOxis], linestyle=lines[nOxis])
            ax1.plot(ox.dosis, np.array(valuesForDosis)*ox.length, color=color[nOxis], label=(ox.name))
            legend_elements.append(Line2D([0], [0], c=color[nOxis], marker=markers[nOxis], label=ox.name))
            nOxis+=1
            fname+=ox.name
            fname+=','
        ax1.set_title("Different oxides - Bias V: "+str(biasV)+"V", fontsize=16)
        ax1.set_ylabel("Interstrip resistance [Ω·cm]", fontsize=12)
        ax1.set_xlabel("Radiation Dose [kGy]", fontsize=12)
        ax1.set_ylim([0.5e8, 1e12])
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(handles=legend_elements, loc="upper right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"resistance/"+fname+"_"+str(biasV)+"V_RD_Curve"+'.png')
        plt.close(fig)



    '''
    IVs
    '''

    def plotIV_Bias(self, ox, dosis, biasV):
        
        fig, ax1 = plt.subplots()
        color = plt.cm.jet(np.linspace(0,1,len(biasV)))
        for v in range(len(biasV)):
            indexDosis = ox.dosis.index(dosis)
            indexBiasV = (np.abs(ox.ivResults[indexDosis,:,0,0] - biasV[v])).argmin()
            ax1.errorbar(ox.ivResults[indexDosis,indexBiasV,1,:], ox.ivResults[indexDosis,indexBiasV,2,:], yerr=ox.ivResults[indexDosis,indexBiasV,3,:], fmt='o', color=color[v])
            ax1.plot(ox.ivResults[indexDosis,indexBiasV,1,:], ox.ivResults[indexDosis,indexBiasV,2,:], color=color[v], label=(str(biasV[v])+"V"))

            Gv_line = np.polyval([ox.rvResults[indexDosis,indexBiasV,3], ox.rvResults[indexDosis,indexBiasV,5]], ox.ivResults[indexDosis,indexBiasV,1,:])
            ax1.plot(ox.ivResults[indexDosis,indexBiasV,1,:], Gv_line, color=color[v], linestyle = 'dashed')



        ax1.set_title(ox.name+" - Dosis: "+str(dosis)+"kGy", fontsize=16)
        ax1.set_ylabel("Interstrip current [A]", fontsize=12)
        ax1.set_xlabel("Interstrip voltage [V]", fontsize=12)
        #ax1.set_ylim([0.5e8, 1e12])
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        #ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(loc="lower right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"current/"+ox.name+"_"+str(dosis)+"kGy_IV_Curve"+'.png')
        plt.close(fig)


    def plotIV_Dosis(self, ox, dosis, biasV):
        
        fig, ax1 = plt.subplots()
        color = plt.cm.jet(np.linspace(0,1,len(dosis)))
        for d in range(len(dosis)):
            indexDosis = ox.dosis.index(dosis[d])
            indexBiasV = (np.abs(ox.ivResults[indexDosis,:,0,0] - biasV)).argmin()
            ax1.errorbar(ox.ivResults[indexDosis,indexBiasV,1,:], ox.ivResults[indexDosis,indexBiasV,2,:], yerr=ox.ivResults[indexDosis,indexBiasV,3,:], fmt='o', color=color[d])
            ax1.plot(ox.ivResults[indexDosis,indexBiasV,1,:], ox.ivResults[indexDosis,indexBiasV,2,:], color=color[d], label=(str(dosis[d])+"kGy"))
            Gv_line = np.polyval([ox.rvResults[indexDosis,indexBiasV,3], ox.rvResults[indexDosis,indexBiasV,5]], ox.ivResults[indexDosis,indexBiasV,1,:])
            ax1.plot(ox.ivResults[indexDosis,indexBiasV,1,:], Gv_line, color=color[d], linestyle = 'dashed')
        ax1.set_title(ox.name+" - Bias voltage: "+str(biasV)+"V", fontsize=16)
        ax1.set_ylabel("Interstrip current [A]", fontsize=12)
        ax1.set_xlabel("Interstrip voltage [V]", fontsize=12)
        #ax1.set_ylim([0.5e8, 1e12])
        ax1.tick_params(axis='both', which='major', labelsize=11)
        ax1.tick_params(axis='both', which='minor', labelsize=11)
        #ax1.tick_params(axis='y', labelcolor=color)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.grid()
        #ax1.set_yscale('log')
        fig.tight_layout()
        plt.legend(loc="lower right", prop={'size': 12})
        #plt.show()
        fig.savefig(self.route+"current/"+ox.name+"_"+str(biasV)+"V_IV_Curve"+'.png')
        plt.close(fig)